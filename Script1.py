import struct
import socket
import time

sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sk.connect(("127.0.0.1",1337))

def recv_pkt():       #Parser le packet reçu selon le format identifié
    data = bytes([])
    while len(data) <7:
        data += sk.recv(1024)

    valid = data[0]
    cmd = data[1]
    size = data[5]
    err = data[6]

    while len(data) <size:
        data += sk.recv(1024)
    return data

def find_length_command(c, n):   #Donne la longueur d'une commande
    
    for i in range(n):
        
        sk.send(bytes(c+"a"*i, "utf-8"))
        data = recv_pkt()
        
        if not b"BAD PACKET LENGTH" in data:
            print("Value {} pour {}".format(data, i))

sk.send(bytes("%0338d348", "utf-8"))
print(recv_pkt())
find_length_command("@", 100)



"""
n = 5   
key_found = ""

for k in range(0, 8):
    
    L = [0]*16
    
    for j in range(n):
        for i in range(16):
            msg = b"%"+bytes(key_found, "utf-8")+bytes(hex(i)[2:], "utf-8")+b"0"*(8-k-1)
            sk.send(msg)
            t = time.time()
            data = recv_pkt()
            t2 = time.time()-t
            #print(t2)
            L[i] += t2
    
    for i in range(len(L)):
        L[i] /= n
        
    maxi = max(L)
    
    for i in range(len(L)):
        if L[i] == maxi:
            print(i)
            key_found += hex(i)[2:]
            print("clé actuelle {}".format(key_found))
            break
"""